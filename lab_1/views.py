from django.shortcuts import render

# Enter your name here
mhs_name = 'Fauzan' # TODO Implement this
mhs_birth_year = 1998

# Create your views here.
def index(request):
    umur = calculate_age(mhs_birth_year)
    response = {'name': mhs_name, 'umur' : umur}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    umur = 2017 - birth_year
    return umur
    
